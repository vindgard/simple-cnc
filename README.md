# SIMPLE-CNC 

[Instructable - 3D printed CNC mill](http://www.instructables.com/id/3D-printed-CNC-mill/?ALLSTEPS)

## Delar att köpa

| Status      | Vad                                          | Antal   | Pris   |
| ----------- | -------------------------------------------- | :-----: | -----: |
| Klar        | [LM8UU 8mm Linear motion bearing][LM8UU]     | 4       | $5.84  |
| Klar        | [LM12UU 12mm Linear motion bearing][LM12UU]  | 8       | $11.68 |
| Klar        | DC5V 2 relay driver module                   | 1       |        |
| Klar        | [NEMA17 Stepper Motor 2 Phase 1.8°][936557]  | 3       | 320:-  |
| Klar        | [5x8 mm stepper motor Coupler][918476]       | 3       | 78:-   |
| Best 8/8    | [3.17mm shaft drill chuck, 2 sizes][ebay1]   | 2       | 20:-   |
| Klar        | EasyDriver Stepper Motor V44 A3967           | 3       |        |
| Klar        | Bearing 608 2RS                              | 10      |        |
|             | Powerbrick 19v 6.3A                          | 1       |        |
| Klar        | Arduino UNO R3                               | 1       |        |
|             | M3 * 30mm Standoffs                          | 10      |        |
|             | M3 * 16mm                                    | 10      |        |
|             | M3 * 20mm                                    | 10      |        |
|             | M3 * 25mm                                    | 10      |        |
|             | M3 * 30mm                                    | 10      |        |
|             | M3 * 6st nylon screws                        |         |        |
|             | Fly nuts (??)                                |         |        |
| Klar        | M8 * 1.25 Brass Nuts                         | 6       |        |
| Klar        | M8 * 1.25 Steel Nuts                         | 62      |        |
| Klar        | M8 Brickor                                   | 24      |        |
| Klar        | M8 * 1.25 Stainless Threaded rod 420         | 2       |        |
| Klar        | M8 * 1.25 Stainless Threaded rod 130         | 1       |        |
|             | M3 6mm nylon                                 | 14      |        |
|             | M3 10mm standoff                             | 6       |        |
|             | M3 30mm standoff                             | 18      |        |
| Hälften     | M3 Nuts                                      | 200     |        |
|             | M3 10mm                                      | 24      |        |
|             | M3 15mm                                      | 44      |        |
|             | M3 20mm                                      | 38      |        |
|             | M3 25mm                                      | 20      |        |
|             | M3 30mm                                      | 20      |        |
| Klar        | M3 Washes                                    | 100     |        |
| Klar        | 12mm slipad axel 400mm                       | 4       |        |
| Klar        | 8mm slipad axel 110mm                        | 2       |        |
| Klar        | Aluprofil 10x10x1.6mm 400mm                  | 4       |        |

## 2 Print

### Delar att printa

Listan nedan bygger på AlexCphotos kompletta kit med alla 3D-delar som nu finns på Thingiverse: [3D Printed & Laser Cut, Lil-CNC Mill ACv2](http://www.thingiverse.com/thing%3A826098)



| Status     | Instruktion         | Vad                                                     | Antal |
|------------|---------------------|---------------------------------------------------------|------:|
| Klar       | Step 2, 10, 13      | Rod clamp / 12 mm Rod clamp                             | 8     |
| Klar       | Step 3, 10, 13      | Bearing Block / Bearing block (alt. 1 av 3, AlexCphoto) | 4     |
| Klar       | Step 4, 13, 14      | Rod holder / 8 mm Rod frame mount                       | 12    |
| Klar       | Step 5, 15          | Linear bearing mount / Y 12 mm Linear bearing sleeve    | 4     |
| Klar       | Step 6, 9, 17, 19   | Anti backlash nut / Backlash nut holder                 | 3     |
| Klar       | Step 7, 12          | Spindle motor mount / 37 mm Spindle motor mount         | 1     |
| Klar       | Step 8, 11          | Z Tool slide                                            | 1     |
| Klar       | Step 9, 11          | X Axis / X Carriage                                     | 1     |
| Klar       | Step 9, 11          | X Axis / X 12 mm Linear bearing sleeve                  | 1     |
| Klar       | Step 15             | Clamp / Table clips                                     | 6     |
| Redesign   | Step 10, 11, 18, 19 | Motor standoffs                                         | 3     |
| Design     | *Step 15*           | Table nut grid (2 parts to replace lasercut)            | 1     |
| Design     | *Step 7, 12*        | Spindle motor mount for Dremel                          | 1     |

### Alternativa delar / optional

| Status     | Instruktion         | Vad                                                     | Antal |
|------------|---------------------|---------------------------------------------------------|------:|
|            | Step 3, 10, 13      | Bearing Block / Solid bearing block (alt. 2 av 3, AlexCphoto)       | 4     |
| Testprint  | Step 3, 10, 13      | Bearing Block / Spring bearing block (alt. 3 av 3)      | 4     |
|            | -                   | Spindle motor control (syns på AlexCphotos bilder)                  | 1     |
|            | Thingiverse/BenRowland | [Dremel 300 Mount For CNC](http://www.thingiverse.com/thing:649179) | 1     |

### Mått & plattor

Prioförklaring. Prio 1 är för att få ihop själva konstruktionen, de andra delarna är mer nice to have och något vi behöver först när vi har kommit en bit på vägen.

| Prio |           Filnamn            |  Mått i mm  |                            Vad                             | Antal |
|------|------------------------------|-------------|------------------------------------------------------------|-------|
|      | `back plate with holes.dxf`  | 380x158     | *Den övre sluttande delen som håller elektroniken*         |     1 |
|      | `deck bottom plate.dxf`      | 345x200     | *Skärbrädan*                                               |     1 |
|      | `deck plate center.dxf`      | 345x200     | *Skärbrädan*                                               |     1 |
|      | `deck plate top.dxf`         | 345x200     | *Skärbrädan*                                               |     1 |
|      | `elecronics board.top 2.dxf` | 233.125x106 | *Elektronikplatta* 1/2                                     |     1 |
|      | `elecronics board.top 2.dxf` | 220x94      | *Elektronikplatta* 2/2                                     |     1 |
|      | `eletronics sides.2.dxf`     | 227.12x48   | *Elektronikplatta* 1/4                                     |     1 |
|      | `eletronics sides.2.dxf`     | 227.12x48   | *Elektronikplatta* 2/4                                     |     1 |
|      | `eletronics sides.2.dxf`     | 100x48      | *Elektronikplatta* 3/4                                     |     1 |
|      | `eletronics sides.2.dxf`     | 100x48      | *Elektronikplatta* 4/4                                     |     1 |
|    1 | `front plate 6mm.dxf`        | 394x100     | *Frontplatta & bakplatta*                                  |     2 |
|    1 | `side plate short.dxf`       | 382x264.005 | *Sidoplatta kort z-axel* **Man väljer short eller tall**   |     2 |
|      | `side plate tall .dxf`       | 382x264.005 | *Sidoplatta längre z-axel* **Man väljer short eller tall** |     2 |
    

### Övrigt

- Beställda fräsdon. [Drill bits](http://www.banggood.com/10pcs-Tungsten-Steel-Hard-Alloy-PCB-CNC-Drill-Bits-For-Circuit-Boards-p-975171.html)

-------

[LM8UU]: http://www.banggood.com/LM8UU-8mm-Linear-Ball-Bearing-Bush-Steel-for-CNC-Router-Mill-Machine-p-906777.html
[LM12UU]: http://www.banggood.com/Wholesale-12mm-Rubber-Sealed-Shielded-Linear-Ball-Bear-Bearing-LM12UU-p-62653.html
[918476]: http://www.banggood.com/5-x-8mm-Flexible-Z-Axes-Motor-Shaft-Coupler-Fix-For-3D-Printer-RepRap-p-918476.html
[936557]: http://www.banggood.com/JKM-NEMA17-Hybrid-Stepper-Motor-2-Phase-1_8-For-CNC-Router-p-936557.html
[ebay1]: http://www.ebay.com.au/itm/2mm-3-17-mm-Electric-Mini-Motor-Shaft-Clamp-Fixture-Chuck-For-0-7mm-3-2mm-Drill-/131483061988?var=430852058133